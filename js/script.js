News.addArticleAction(function ($actionsElement, article) {
    let $li = $('<li>')
        .addClass('article-plugin-bookmarks');
    let $button = $('<button>')
        .attr('title', t('newsplugin', 'Add to Bookmarks'));

    $button.click(function (event) {
        let url = window.location.origin + '/apps/bookmarks/bookmarklet?url=' + encodeURIComponent(article.url) + '&title=' + encodeURIComponent(article.title);
        let options = 'left=' + ((window.screenX || window.screenLeft) + 50) +
            ',top=' + ((window.screenY || window.screenTop) + 50) +
            ',height=600px' +
            ',width=550px' +
            ',resizable=1' +
            ',alwaysRaised=1';
        window.setTimeout(function () {
            window.open(url, 'bkmk_popup', options).focus()
        }, 300);

        event.stopPropagation();  // prevent expanding in compact mode
    });

    $li.append($button);
    $actionsElement.append($li);
});
