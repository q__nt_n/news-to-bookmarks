<?php
namespace OCA\MyNewsPlugin\AppInfo;
use OCP\App;

if (App::isEnabled('news') && App::isEnabled('bookmarks') && class_exists('OCA\News\Plugin\Client\Plugin')) {
	\OCA\News\Plugin\Client\Plugin::registerScript('newstobookmarks', 'script');
	\OCA\News\Plugin\Client\Plugin::registerStyle('newstobookmarks', 'style');	
}
